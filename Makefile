.PHONY: all faq

all: faq links

links: links.html

links.html: links.xml
	-mv links.html links.html.old
	if ./make-links.pl links.xml >links.html; then \
	  rm -f links.html.old; \
	else \
	  -mv links.html.old links.html; \
	fi

faq: faq.html

faq.html: faq.src
	mv faq.html faq.html.old
	if ./make-faq.pl <faq.src >faq.html; then \
	  rm faq.html.old; \
	else \
	  mv faq.html.old faq.html; \
	fi
